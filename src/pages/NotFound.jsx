import React from "react";
import { NavLink } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="text-center">
      <h1 className="my-16 text-5xl text-red-500">404 Page - NotFound</h1>
      <NavLink className="bg-blue-400 p-3 text-white rounded-md" to={"/"}>
        Go to Home
      </NavLink>
    </div>
  );
};

export default NotFound;
