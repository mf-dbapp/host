// import PropTypes from 'prop-types'
// import React, { Component } from 'react'

// export class Error extends Component {
//   static propTypes = {}

//   render() {
//     return (
//       <div>Error</div>
//     )
//   }
// }

// export default Error
import React, { Component } from "react";

export class Error extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {}

  render() {
    if (this.state.hasError) {
      return (
        <div>
          <h2>Ocurrió un error en el Micro-Frontend, favor de validarlo</h2>
          {/* Mostrar más detalles sobre el error si lo deseas */}
          {this.state.error && <p>{this.state.error.toString()}</p>}
          {this.state.errorInfo && (
            <details style={{ whiteSpace: "pre-wrap" }}>
              {this.state.errorInfo.componentStack}
            </details>
          )}
        </div>
      );
    }
    return this.props.children;
  }
}
